outfile  = mux
outTB = $(outfile)_tb

infile  = $(outfile).vhdl
inTB = $(outTB).vhdl

GFLAGS = -g --std=08 -frelaxed-rules -fsynopsys --ieee=standard

all: $(outfile)

$(outfile): $(infile) $(inTB)
	ghdl -a $(GFLAGS) $(infile)
	ghdl -a $(GFLAGS) $(inTB)
	ghdl -e $(GFLAGS) $(outfile)
	ghdl -e $(GFLAGS) $(outTB)
	
clean:
	rm -f *.cf
	rm -f *.o
	rm -f $(outfile)
	rm -f $(outTB)
	rm -f waveform.vcd

run: $(outTB)
	@echo "--------- START OF OUTPUT --------"
	@ghdl -r --std=08 $(outTB) --stop-time=1us --vcd="waveform.vcd"
	@echo "--------- END OF OUTPUT --------"

dev: clean run
