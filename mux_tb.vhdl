library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux_tb is
end entity;

architecture sim of mux_tb is
	-- specify a component
	component mux
		port(
			Sig1,Sig2,Sig3,Sig4 :  in unsigned(7 downto 0);
			Selector 			:  in unsigned(1 downto 0);
			Output   			: out unsigned(7 downto 0)
		);
	end component;

	-- bind testbench component's signals with the ones in mux
	for mux_component: mux use entity work.mux;

	-- specify the signals
	signal Sig1 : unsigned(7 downto 0) := x"AA";
	signal Sig2 : unsigned(7 downto 0) := x"BB";
	signal Sig3 : unsigned(7 downto 0) := x"CC";
	signal Sig4 : unsigned(7 downto 0) := x"DD";

	signal Selector : unsigned(1 downto 0) := "00";
	
	signal Output : unsigned(7 downto 0);

begin
	-- initalize component mux as mux_component
	mux_component: mux port map (Sig1 => Sig1, Sig2 => Sig2,
								 Sig3 => Sig3, Sig4 => Sig4,
								 Selector => Selector, Output => Output
							 	);
	-- testbench process
	process is
	begin
		wait for 10 ns;
		Selector <= Selector + 1;
		wait for 10 ns;
		Selector <= Selector + 1;
		wait for 10 ns;
		Selector <= Selector + 1;
		wait for 10 ns;
		Selector <= Selector + 1;
		wait for 10 ns;
		wait;		

	end process;
end architecture;
